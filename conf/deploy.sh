#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp wiki.org/named.conf dwikiorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp wiki.org/* dwikiorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf 


# diutre
himage diutre mkdir -p /etc/named

hcp iut.re/named.conf diutre:/etc/.

hcp iut.re/* diutre:/etc/named/.

himage diutre rm /etc/named/named.conf


# drtiutre
himage drtiutre mkdir -p /etc/named

hcp rt.iut.re/named.conf drtiutre:/etc/.

hcp rt.iut.re/* drtiutre:/etc/named/.

himage drtiutre rm /etc/named/named.conf


# dorg
himage dorg mkdir -p /etc/named

hcp dorg/named.conf dorg:/etc/.

hcp dorg/* dorg:/etc/named/.

himage dorg rm /etc/named/named.conf




# dre
himage dre mkdir -p /etc/named

hcp dre/named.conf dre:/etc/.

hcp dre/* dre:/etc/named/.

himage dre rm /etc/named/named.conf



# arootserver
himage aRootServer mkdir -p /etc/named

hcp arootserver/named.conf aRootServer:/etc/.

hcp arootserver/* aRootServer:/etc/named/.

himage aRootServer rm /etc/named/named.conf





## configuration de iut.re 
# configuration du serveur dns
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#himage diutre mkdir -p /etc/named
#hcp iut.re/named.conf diutre:/etc/.
#hcp iut.re/* diutre:/etc/named/.
#himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#hcp pc1/resolv.conf pc1:/etc/.
